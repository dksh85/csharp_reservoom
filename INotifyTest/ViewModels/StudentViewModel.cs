﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INotifyTest.ViewModels
{
    internal class StudentViewModel : INotifyPropertyChanged
    {
        Models.StudentModel studentModel = null;

        public StudentViewModel()
        {
            studentModel = new Models.StudentModel();
        }

        public Models.StudentModel StudentModel
        {
            get
            {
                return studentModel;
            }
            set
            {
                studentModel = value;
            }
        }

        public string Name
        {
            get
            {
                return StudentModel.Name;
            }
            set
            {
                if (StudentModel.Name != value)
                {
                    StudentModel.Name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        public string PhoneNumber
        {
            get
            {
                return StudentModel.PhoneNumber;
            }
            set
            {
                if (StudentModel.PhoneNumber != value)
                {
                    StudentModel.PhoneNumber = value;
                    OnPropertyChanged("PhoneNumber");
                }
            }
        }

        //PropertyChaneged 이벤트 선언 및 이벤트 핸들러
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
