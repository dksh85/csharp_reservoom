﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BindingTest.Models
{
    public class Product
    {
        public string Name { get; }
        public int Price { get; }
        public int Quantity { get; }
        
        public Product(string name, int price, int quantity)
        {
            Name = name;
            Price = price;
            Quantity = quantity;
        }
    }
}
