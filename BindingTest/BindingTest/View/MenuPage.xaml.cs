﻿using BindingTest.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BindingTest.View
{
    /// <summary>
    /// MenuPage.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MenuPage : Page
    {
        public MenuPage()
        {
            InitializeComponent();
        }
        
        private void EmailSupp(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(
                new Uri("/View/Contacts.xaml", UriKind.Relative));
            
        }

        private void ProductItem(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(
                new Uri("/View/ListItem.xaml", UriKind.Relative));
            
        }
    }
}
