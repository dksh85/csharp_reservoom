﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BindingTest.View
{
    /// <summary>
    /// Contacts.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class Contacts : Page
    {
        public Contacts()
        {
            InitializeComponent();
        }

        private void To_Main_Btn(object obj, RoutedEventArgs e )
        {
            this.NavigationService.Navigate(
                    new Uri("/View/MenuPage.xaml", UriKind.Relative)
            );
        }
    }
}
