﻿using System;
using System.Collections.Generic;
using System.Text;
using BindingTest.Models;

namespace BindingTest.ViewModels
{
    internal class ProductViewModel : ViewModelBase
    {
        private Product _product;

        public string Name => _product.Name?.ToString();
        public int Price => _product.Price;
        public int Quantity => _product.Quantity;

        public ProductViewModel(Product product)
        {
            _product = product;
        }
    }

}
