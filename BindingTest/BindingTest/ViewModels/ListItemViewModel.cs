﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using BindingTest.Models;

namespace BindingTest.ViewModels
{
    internal class ListItemViewModel : ViewModelBase
    {
        private ObservableCollection<ProductViewModel> _products;

        public IEnumerable<ProductViewModel> Products => _products;

        public ListItemViewModel()
        {
            _products = new ObservableCollection<ProductViewModel>();
            _products.Add(new ProductViewModel(new Product("Apple", 1200, 12)));
            _products.Add(new ProductViewModel(new Product("Lemon", 1000, 10)));
            _products.Add(new ProductViewModel(new Product("Mango", 1500, 10)));

        }
          
    }
}
