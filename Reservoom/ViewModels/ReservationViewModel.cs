﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reservoom.Models;

namespace Reservoom.ViewModels
{
    internal class ReservationViewModel : ViewModelBase
    {
        private readonly Reservation _reservation;

        public string RoomID => _reservation.RoomID?.ToString(); //lambda 식 : RoomID를 _reservation.RoomID?.ToString() 으로 반환
        public string Username => _reservation.Username;
        public string StartTime => _reservation.StartTime.ToString();
        public string EndTime => _reservation.EndTime.ToString();

        public ReservationViewModel(Reservation reservation)
        {
            _reservation = reservation;
        }
    }
}
