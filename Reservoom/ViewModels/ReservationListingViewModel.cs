﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Reservoom.ViewModels
{
    internal class ReservationListingViewModel : ViewModelBase
    {
        private readonly ObservableCollection<ReservationViewModel> _reservations;

        public IEnumerable<ReservationViewModel> Reservations => _reservations; // ReservationListingView.xaml 에 binding 되어 있다.
        /* ReservationListingView.xaml 에는 ListView 가 시작되는 지점에 ItemsSource="{Binding Reservations}" 가 명시되어 있고
         * 각각의 TextBlock 에는 TextBlock="{Binding <변수>}" 으로 명시되어있다.
         * 여기서 <변수>는 Reservations의 해당되는것이고 Reservations은 ReservationViewModel 로 이루어져있기 때문에
         * ReservationViewModel의 변수를 참조하게 된다.
         * 
         */
        public ICommand MakeReservationCommand { get; }

        public ReservationListingViewModel()
        {
            _reservations = new ObservableCollection<ReservationViewModel>();
            _reservations.Add(new ReservationViewModel(new Models.Reservation(new Models.RoomID(1, 2), "SingletonSean", DateTime.Now, DateTime.Now)));
            _reservations.Add(new ReservationViewModel(new Models.Reservation(new Models.RoomID(3, 2), "Joe", DateTime.Now, DateTime.Now)));
            _reservations.Add(new ReservationViewModel(new Models.Reservation(new Models.RoomID(2, 2), "Mary", DateTime.Now, DateTime.Now)));
        }
    }
}
