﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reservoom.ViewModels
{
    internal class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        /*
        현재 ViewModelBase(INotifyProportyChanged 인터페이스)를 사용되는 UI에 binding된 변수를 알림.
         */

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            /* \
             * UI에게 Property가 바뀌었다는 것을 알리기위한 함수
            */
        }
    }
}
